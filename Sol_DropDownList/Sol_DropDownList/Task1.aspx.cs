﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_DropDownList
{
    public partial class Task1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            var colorObj = txtColor.Text;


            //if(!string.IsNullOrEmpty(colorObj))
            if (colorObj != null)
            {
                ddlColor.Items.Add(new ListItem(colorObj, colorObj));
            }
        }
    }
}