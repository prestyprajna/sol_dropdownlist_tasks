﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Task2.aspx.cs" Inherits="Sol_DropDownList.Task2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManger1" runat="server">
        </asp:ScriptManager>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <table>

                    <tr>
                        <td>
                              <asp:DropDownList ID="ddlColor" runat="server">

                                <asp:ListItem Value="1" Text="Red"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Blue"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Yellow"></asp:ListItem>
                                <asp:ListItem Value="4" Text="Green"></asp:ListItem>

                             </asp:DropDownList>                            
                        </td>

                        <td>
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                        </td>
                    </tr>

                    <tr>                       
                        <td>
                            <div>
                                <asp:Image ID="imgColor" runat="server" Height="100px" Width="100px" ToolTip="I Display Selected Color" />
                            </div>
                        </td>
                    </tr>  
                    
                    <tr>
                        <td>
                            <asp:Label ID="lblSelectedIndexValue" runat="server"></asp:Label>
                        </td>
                    </tr>                

                </table>      
              
            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
