﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Task3.aspx.cs" Inherits="Sol_DropDownList.Task3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="scriptManager1" runat="server">
        </asp:ScriptManager>
    
        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate>
                <table>

                    <tr>
                        <td>
                            <asp:Label ID="lblFirstName" runat="server" Text="FirstName :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                        </td>
                    </tr>

                     <tr>
                         <td>
                            <asp:Label ID="lblLastName" runat="server" Text="LastName :"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                        </td>
                    </tr>

                     <tr>
                         <td>
                            <asp:Label ID="lblCityName" runat="server" Text="City :"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCity" runat="server" DataTextField="CityName" DataValueField="CityId"></asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:Button ID="btnInsert" runat="server" Text="INSERT"  OnClick="btnInsert_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnEdit" runat="server" Text="EDIT" OnClick="btnEdit_Click" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </td>
                    </tr>

                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
