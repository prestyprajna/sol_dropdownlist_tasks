﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Task1.aspx.cs" Inherits="Sol_DropDownList.Task1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <table>
                    <tr>
                        <td>
                             <asp:TextBox ID="txtColor" runat="server" placeholder="Color"></asp:TextBox>
                        </td>

                        <td>
                             <asp:Button ID="btnAdd" runat="server" Text="Add Item" OnClick="btnAdd_Click" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:DropDownList ID="ddlColor" runat="server">
                                <asp:ListItem Text="--Colors--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>  

            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
