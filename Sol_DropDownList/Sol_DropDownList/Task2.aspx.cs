﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_DropDownList
{
    public partial class Task2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                ddlColor.DataBind();

                ddlColor.AppendDataBoundItems = true;
                ddlColor.Items.Insert(0, new ListItem("--Select Color--", "0"));

            }                        
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string colorObj = ddlColor.SelectedItem.Value;

            if(colorObj=="1")
                imgColor.Style["background-color"] = "Red";
            else if(colorObj == "2")
                imgColor.Style["background-color"] = "Blue";
            else if(colorObj == "3")
                imgColor.Style["background-color"] = "Yellow";
            else if(colorObj == "4")
            imgColor.Style["background-color"] = "Green";

            //lblSelectedIndexValue.Text = colorObj;

        }


    }
}