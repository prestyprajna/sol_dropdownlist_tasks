﻿using Sol_DropDownList.Models.Entity;
using Sol_DropDownList.Models.ORD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_DropDownList.Models.DAL
{
    public class UserDal
    {
        #region  declaration
        private UserDCDataContext _dc = null;
        #endregion

        #region  constructor
        public UserDal()
        {
            _dc = new UserDCDataContext();
        }

        #endregion

        #region  public methods

        public string AddUserData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            var setQuery=_dc
                ?.uspSetUser(
                    "INSERT",
                    userEntityObj?.UserId,
                    userEntityObj?.FirstName,
                    userEntityObj?.LastName,
                    Convert.ToDecimal(userEntityObj?.CityId),
                    ref status,
                    ref message
                    );

            // return (status == 1) ? true : false;

            return message;


        }

        public string UpdateUserData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;

            var setQuery = _dc
                ?.uspSetUser(
                    "UPDATE",
                    userEntityObj?.UserId,
                    userEntityObj?.FirstName,
                    userEntityObj?.LastName,
                    Convert.ToDecimal(userEntityObj?.CityId),
                    ref status,
                    ref message
                    );

            // return (status == 1) ? true : false;

            return message;


        }

        public List<UserEntity> GetCityName()
        {
            var setQuery = _dc
                ?.tblCities
                ?.AsEnumerable()
                ?.Select((leCityObj) => new UserEntity()
                {
                   // CityId = leCityObj?.CityId,
                    CityName = leCityObj?.CityName
                })
                ?.ToList();

            return setQuery;

        }

        #endregion
    }
}