﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_DropDownList.Models.Entity
{
    public class UserEntity
    {
        public decimal? UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CityId { get; set; }

        public string CityName { get; set; }


    }
}