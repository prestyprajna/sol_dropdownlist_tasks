﻿using Sol_DropDownList.Models.DAL;
using Sol_DropDownList.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_DropDownList
{
    public partial class Task3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(IsPostBack==false)
            {
                this.BindCities();                
            }
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            UserEntity userEntityObj = new UserEntity()
            {
                FirstName = lblFirstName.Text,
                LastName = lblLastName.Text,
                CityId = ddlCity.SelectedItem.Value,
                CityName=ddlCity.SelectedItem.Text
            };

            string message = new UserDal().AddUserData(userEntityObj);

            lblMessage.Text = message;
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            UserEntity userEntityObj = new UserEntity()
            {
                FirstName = lblFirstName.Text,
                LastName = lblLastName.Text,
                CityId = ddlCity.SelectedItem.Value,
                CityName = ddlCity.SelectedItem.Text
            };

            string message = new UserDal().UpdateUserData(userEntityObj);

            lblMessage.Text = message;
        }

        private void BindCities()
        {
            var citiesObj = new UserDal().GetCityName();

            ddlCity.DataSource = citiesObj;
            ddlCity.DataBind();

            ddlCity.AppendDataBoundItems = true;
            ddlCity.Items.Insert(0, new ListItem("--select city--", "0"));
        }
    }
}