﻿CREATE TABLE [dbo].[tblUser] (
    [UserId]    NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [LastName]  VARCHAR (50) NULL,
    [CityId]    NUMERIC (18) NOT NULL,
    PRIMARY KEY CLUSTERED ([UserId] ASC)
);

