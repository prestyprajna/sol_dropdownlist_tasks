﻿CREATE PROCEDURE uspSetUser
(
	@Command VARCHAR(MAX)=NOTNULL,

	@UserId NUMERIC(18,0)=NOTNULL,
	@FirstName VARCHAR(50)=NULL,
	@LastName VARCHAR(50)=NULL,

	@CityId NUMERIC(18,0)=NOTNULL,
	--@CityName VARCHAR(50)=NULL
	
	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
		
)
AS
	BEGIN

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

		IF @Command='INSERT'
			BEGIN

			BEGIN TRANSACTION

			BEGIN TRY
				INSERT INTO tblUser
				(
				FirstName,
				LastName,
				CityId
				)
				VALUES
				(
				@FirstName,
				@LastName,
				@CityId
				)

				SET @Status=1
				SET @Message='INSERT SUCCESSFULL'

					COMMIT TRANSACTION
			END TRY

			BEGIN CATCH 
			
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				SET @Status=0
				SET @Message='INSERT EXCEPTION'

				RAISERROR(@ErrorMessage,16,1)

			END CATCH

			END

		ELSE IF @Command='Update'
			BEGIN

			BEGIN TRANSACTION

				BEGIN TRY

				SELECT 			
				@FirstName=CASE WHEN @FirstName IS NULL THEN U.FirstName ELSE @FirstName END,
				@LastName=CASE WHEN @LastName IS NULL THEN U.LastName ELSE @LastName END,
				@CityId=CASE WHEN @CityId IS NULL THEN U.CityId ELSE @CityId END
				FROM tblUser AS U

				UPDATE tblUser
					SET FirstName=@FirstName,
					LastName=@LastName,
					CityId=@CityId
						WHERE UserId=@UserId

				SET @Status=1
				SET @Message='UPDATE SUCCESSFULL'

					COMMIT TRANSACTION
			END TRY

			BEGIN CATCH 
			
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				SET @Status=0
				SET @Message='UPDATE EXCEPTION'

				RAISERROR(@ErrorMessage,16,1)

			END CATCH


			END

	END
